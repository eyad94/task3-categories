<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	
    protected $guarded = ['id'];

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id','id');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id','id');
    }
    
    public function getParentsNames() {
    if($this->parent) {
        return $this->parent->getParentsNames(). " > " . $this->name;
    } else {
        return $this->name;
    }
     }


}
