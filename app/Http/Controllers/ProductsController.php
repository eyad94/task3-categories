<?php

namespace App\Http\Controllers;
use App\Product;
use App\Category;
use Illuminate\Http\Request;

class ProductsController extends Controller
{

public function index(Product $product){

      $categories = Category::where('id',$product->cat_id)->get();
      $t;
      $i=0;
      foreach ($categories as $category) {
      
      	   $t[$i]=  $category->getParentsNames();
      	  $i++;
      }

     return \response()->json([
     	 'Product_Name' => $product->name,
         'Categorys:' => $t
       
       ]);

         
    }
}
