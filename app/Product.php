<?php

namespace App;
use App\Product;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function Categories()
    {
    	return $this->belongsTo('App\Category','id');
    }
}
