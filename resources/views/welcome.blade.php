<!DOCTYPE html>
<html>
<head>
    <title>Products</title>
</head>
<body>
                <strong>Product Name:</strong> {{ $product->name }}<br>
                <strong>Product Category:</strong> 
                     
                  @foreach ($categories as $category)
                    {{ $category->getParentsNames() }}
                @include('category', $category)

               @endforeach

</body>
</html>